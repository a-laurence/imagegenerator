// Process.cpp: Implementation for image processing functions
// 
// Author: L. Abanes | September 2020
//
//////////////////////////////////////////////////////////////////////

#pragma warning(disable : 26451)

#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <filesystem>
#include <random>
#include <future>
#include <thread>
#include <vector>
#include <ctime>
#include <string>
#include <sstream>

#include "XmlWriter.h"
#include "Progress.h"
#include "Process.h"
#include "Image.h"
#include "Util.h"

using namespace util;
namespace fs = std::filesystem;


std::random_device rd;
std::mt19937 gen(rd());
std::uniform_real_distribution<> dis(proc::SCALE_MIN, proc::SCALE_MAX);


cv::String proc::getRandomImage(Image& img)
{
	auto it = img.fn.cbegin();
	int r_num = rand() % img.count;
	std::advance(it, r_num);

	return *it;
}


cv::Mat proc::rotateImage(cv::Mat original, int angle)
{
	cv::Point2f center((original.cols - 1) / 2.0, (original.rows - 1) / 2.0);
	cv::Mat rotation = cv::getRotationMatrix2D(center, angle, 1.0);
	cv::Rect2f box = cv::RotatedRect(cv::Point2f(), original.size(), angle).boundingRect2f();

	rotation.at<double>(0, 2) += box.width / 2.0 - original.cols / 2.0;
	rotation.at<double>(1, 2) += box.height / 2.0 - original.rows / 2.0;

	cv::Mat rotImage;
	cv::warpAffine(original, rotImage, rotation, box.size());
	return rotImage;
}


cv::Mat proc::pasteImage(cv::Mat jpg, cv::Mat png, cv::Point2i position)
{
	cv::Mat outImage;
	jpg.copyTo(outImage);
	int Fy = 0, Fx = 0;

	for (int y = std::max(position.y, 0); y < jpg.cols; ++y)
	{
		Fy = y - position.y;
		if (Fy >= png.rows)
			break;

		for (int x = std::max(position.x, 0); x < jpg.cols; ++x)
		{
			Fx = x - position.x;
			if (Fx >= png.cols)
				break;

			double opacity = ((double)png.data[Fy * png.step + Fx * png.channels() + 3]) / 255.;
			for (int c = 0; opacity > 0 && c < png.channels(); ++c)
			{
				unsigned char pngPx = png.data[Fy * png.step + Fx * png.channels() + c];
				unsigned char jpgPx = jpg.data[y * jpg.step + x * jpg.channels() + c];
				outImage.data[y * outImage.step + outImage.channels() * x + c] = jpgPx * (1. - opacity) + pngPx * opacity;
			}
		}
	}
	return outImage;
}


bool proc::genSingleObject(Image& png, Image& jpg, int& count, int& tracker, cv::Point2i& location, Arguments& args, std::string destination, size_t start, size_t end, int p)
{
	cv::Mat intJpg = cv::imread(jpg.fn[0], cv::IMREAD_UNCHANGED);
	int bgHeight = intJpg.rows;
	int bgWidth = intJpg.cols;

	for (size_t i = start; i < end; i++)
	{
		tracker += 1;
		cv::Mat objImage = cv::imread(png.fn[i], cv::IMREAD_UNCHANGED);
		int angle = 0;

		do
		{
			cv::Mat rotImage = rotateImage(objImage, angle);

			int hStart = -(rotImage.cols / 3);
			int wStart = -(rotImage.rows / 3);
			int hEnd = bgHeight - (2 * rotImage.cols / 3);
			int wEnd = bgWidth - (2 * rotImage.rows / 3);
			int hMove = floor(bgHeight / location.y);
			int wMove = floor(bgWidth / location.x);

			if (floor(hMove / 2) < rotImage.cols / 2)
			{
				hStart = 0;
				hEnd = bgHeight - rotImage.cols;
				hMove = floor(hEnd / (location.y - 1));
			}
			else
			{
				hStart = floor(hMove / 2) - rotImage.cols / 2;
				hEnd = bgHeight - floor(hMove / 2) - rotImage.cols / 2;
			}

			if (floor(wMove / 2) < (rotImage.rows / 2))
			{
				wStart = 0;
				wEnd = bgWidth - rotImage.rows;
				wMove = floor(wEnd / (location.x - 1));
			}
			else
			{
				wStart = floor(wMove / 2) - rotImage.rows / 2;
				wEnd = bgWidth - floor(wMove / 2) - rotImage.rows / 2;
			}

			for (int h = hStart; h <= hEnd; h += hMove)
			{
				for (int w = wStart; w <= wEnd; w += wMove)
				{
					cv::Mat resImage;
					cv::Mat bgImage;

					cv::Point position(w, h);
					std::future<cv::Mat> bg = std::async(cv::imread, getRandomImage(jpg), cv::IMREAD_UNCHANGED);
					
					float scale = dis(gen);
					cv::resize(rotImage, resImage, cv::Size(), scale, scale, cv::INTER_LINEAR);
					bgImage = bg.get();

					if (position.y > (bgImage.rows - resImage.rows))
						position.y = 0;

					std::future<cv::Mat> out = std::async(pasteImage, bgImage, resImage, position);
					std::string serial = SERIAL(count, p);
					std::string filename = destination + "objects/" + serial + ".jpg";
					cv::Mat outImage = out.get();
					cv::imwrite(filename, outImage);

					ImageObject img(outImage, filename);
					img.addObject(png.fn[i]);
					img.hmin.push_back(std::max(h, 0));
					img.wmin.push_back(std::max(w, 0));
					img.hmax.push_back(std::min(h + resImage.rows, bgHeight));
					img.wmax.push_back(std::min(w + resImage.cols, bgWidth));

					double sumOut = 0.0;
					double sumRes = 0.0;
					for (int i = 0; i < 3; i++)
					{
						sumOut += cv::sum(outImage)[i];
						sumRes += cv::sum(resImage)[i];					}
					
					img.visibleRate.push_back(sumOut / sumRes);

					Annotation anno(destination + "annotations/" + serial + ".xml");
					addAnnotation(anno, img, args);
				}
			}
			angle += args.step;
		} while (angle <= args.maxRot);
	}
	return true;
}


bool proc::genMultipleObjects(Image& jpg, Image& png, int& count, int& max, Arguments& args, std::string destination, int p)
{
	for (int k = 0; k < max; k++)
	{
		ImageObject imgObject;
		std::vector<cv::Mat> bgTemp;
		cv::Mat bg = cv::imread(getRandomImage(jpg), cv::IMREAD_UNCHANGED);
		bgTemp.push_back(bg);

		int width = bg.cols;
		int height = bg.rows;
		int numObjects = rand() % 3 + 2;

		for (int j = 0; j < numObjects; j++)
		{
			cv::String objStr = getRandomImage(png);

			std::future<cv::Mat> obj = std::async(cv::imread, objStr, cv::IMREAD_UNCHANGED);
			int angle = rand() % 360;
			cv::Mat rotImage = rotateImage(obj.get(), angle);
			cv::Mat resImage;
			float scale = dis(gen);
			cv::resize(rotImage, resImage, cv::Size(), scale, scale, cv::INTER_LINEAR);

			int x = rand() % width;
			int y = rand() % height;
			cv::Point location(x, y);
			if (location.y > (bg.rows - resImage.rows))
				location.y = 0;

			cv::Mat genImage = pasteImage(bgTemp.back(), resImage, location);
			bgTemp.push_back(genImage);

			imgObject.addObject(objStr);
			imgObject.hmin.push_back(std::max(y, 0));
			imgObject.wmin.push_back(std::max(x, 0));
			imgObject.hmax.push_back(std::min(y + resImage.rows, height));
			imgObject.wmax.push_back(std::min(x + resImage.cols, width));

			double sumOut = 0.0;
			double sumRes = 0.0;
			for (int i = 0; i < 3; i++)
			{
				sumOut += cv::sum(genImage)[i];
				sumRes += cv::sum(resImage)[i];
			}
			imgObject.visibleRate.push_back(sumOut / sumRes);
		}
		std::string serial = SERIAL(count, p);
		std::string filename = destination + "objects/" + serial + ".jpg";
		cv::imwrite(filename, bgTemp.back());

		imgObject.addImage(bgTemp.back(), filename);
		Annotation anno(destination + "annotations/" + serial + ".xml");
		addAnnotation(anno, imgObject, args);
	}
	return true;
}


void proc::addAnnotation(Annotation& xml, ImageObject& img, Arguments& args)
{
	xml.createTag("annotation");

	xml.createChild("folder", args.folder);
	xml.createChild("filename", fs::path(img.filepath).filename().string());

	xml.createTag("source");
	xml.createChild("database", args.database);
	xml.createChild("annotation", args.annotation);
	xml.createChild("image", args.image);
	xml.createChild("flickrid", args.flickrid);
	xml.closeLastTag();

	xml.createTag("owner");
	xml.createChild("flickrid", args.ownerflickrid);
	xml.createChild("name", args.ownerName);
	xml.closeLastTag();

	xml.createTag("size");
	xml.createChild("width", std::to_string(img.width));
	xml.createChild("height", std::to_string(img.height));
	xml.createChild("depth", std::to_string(args.depth));
	xml.closeLastTag();

	xml.createChild("segmented", std::to_string(args.segmented));

	for (int i = 0; i < img.objectList.size(); i++)
	{
		xml.createTag("object");
		xml.createChild("name", img.objectName[i]);
		xml.createChild("category", img.objectCategory[i]);
		xml.createChild("pose", img.objectPose[i]);
		xml.createChild("truncated", std::to_string(args.truncated));
		xml.createChild("dificult", std::to_string(args.difficult));
		xml.createChild("visible", std::to_string(img.visibleRate[i]));

		xml.createTag("occlusion");
		xml.createChild("occlusion_cate", img.occlusionCate[i]);
		xml.createChild("occlusion_name", img.occlusionName[i]);
		xml.createChild("occlusion_pose", img.occlusionPose[i]);
		xml.createChild("occlusion_rate", img.occlusionRate[i]);
		xml.closeLastTag();

		xml.createTag("bndbox");
		xml.createChild("xmin", std::to_string(img.wmin[i]));
		xml.createChild("ymin", std::to_string(img.hmin[i]));
		xml.createChild("xmax", std::to_string(img.wmax[i]));
		xml.createChild("ymax", std::to_string(img.hmax[i]));
		xml.closeLastTag();

		xml.closeLastTag();
	}

	xml.closeLastTag();
	xml.cloaseAllTag();
}

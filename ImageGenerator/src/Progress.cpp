﻿// Progress.cpp: Implementation for progress indicator
// 
// Author: L. Abanes | September 2020
//
//////////////////////////////////////////////////////////////////////

#include <iostream>
#include <atomic>
#include <string>

#include "Progress.h"


Progress::Progress()
{
	setBarWidth(50);
	fillBarProgressWith("■");
	fillBarRemainderWith(" ");
}


void Progress::setProgress(float value)
{
	//std::lock_guard<std::recursive_mutex> lock(__mutex);
	__progress = value;
}


void Progress::setBarWidth(size_t width)
{
	//std::lock_guard<std::recursive_mutex> lock(__mutex);
	barWidth = width;
}


void Progress::setStatusText(const std::string& status)
{
	//std::lock_guard<std::recursive_mutex> lock(__mutex);
	statusText = status;
}


void Progress::writeProgress(std::ostream& os)
{
	//std::lock_guard<std::recursive_mutex> lock(__mutex);
	if (__progress > 100.0f) return;
	os << "\r" << std::flush;
	os << "[";
	const auto completed = static_cast<size_t>(__progress * static_cast<float>(barWidth) / 100.0);
	for (size_t i = 0; i < barWidth; ++i)
	{
		if (i <= completed) os << __fill;
		else os << __remainder;
	}
	os << "]";
	os << " " << std::min(static_cast<size_t>(__progress), static_cast<size_t>(100)) << "%";
	os << " " << statusText;
}


void Progress::fillBarProgressWith(const std::string& chars)
{
	//std::lock_guard<std::recursive_mutex> lock(__mutex);
	__fill = chars;
}


void Progress::fillBarRemainderWith(const std::string& chars)
{
	//std::lock_guard<std::recursive_mutex> lock(__mutex);
	__remainder = chars;
}


void Progress::update(float value, std::ostream& os)
{
	setProgress(value);
	writeProgress(os);
}
